const express = require('express');
const router = express.Router();

//READ ALL
router.get('/', function(req, res, next) {
  req.getConnection(function(err, connection) {
    let queryStr = "select * from prices";
    let query = connection.query(queryStr, function(err, rows){
      if (err) {
        console.log('Error: %s', err);
        res.send(err);
      } else {
        res.json(rows);
      }
    });
    console.log(query.sql);
  })
});

//READ by id
router.get('/:id', function(req, res, next){
  const id = req.params.id;
  req.getConnection(function(err, connection){
    let query = connection.query("SELECT * FROM prices WHERE id = ?", id, function(err, rows){
      if (err) {
        console.dir(err);
        res.send(err);
      } else {
        console.dir(rows);
        res.json(rows);
      }
    });
  });
});

//ADD
router.post('/', function(req, res, next){
  let params = JSON.parse(JSON.stringify(req.body));
  req.getConnection(function(err, connection){
    const data = {
      security: params.security,
      price: params.price
    };
    let query = connection.query("INSERT INTO prices set ?", data, function(err, rows){
      if (err) {
        res.send(err);
      } else {
        res.json(rows);
      }
    });
    console.log(query.sql);
  });
});

//UPDATE
router.put('/:id', function(req, res, next) {
  let params = JSON.parse(JSON.stringify(req.body));
  let id = req.params.id;
  req.getConnection(function(err, connection) {
    const data = {
      security: params.security,
      price: params.price
    };

    let query = connection.query("UPDATE prices set ? WHERE id = ? ", [data, id], function(err, rows){
      if (err){
          res.send(err);
      } else {
          res.json(rows);
      }
    });
    console.log(query.sql);
  });
});

//DELETE
router.delete('/:id', function(req, res, next){
  let id = req.params.id;
  req.getConnection(function(err, connection){
      let query = connection.query("DELETE FROM prices WHERE id = ?", id, function(err, rows){
        if (err) {
          res.send(err);
        }else {
          res.json(rows);
        }
      });
  });
});

module.exports = router;
