curl -X PUT --header 'Content-Type: application/json' --header 'Accept: text/html' -d '{"security": "vnm","price": 36.3}' 'http://127.0.0.1:3000/prices/2'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: text/html' -d '{"security": "nut","price": 30.3}' 'http://127.0.0.1:3000/prices'

curl -X DELETE --header 'Content-Type: application/json' --header 'Accept: text/html' -d '{"security": "nut","price": 30.3}' 'http://127.0.0.1:3000/prices/1'

curl -X GET --header 'Accept: text/html' 'http://127.0.0.1:3000/prices/2'
