-- --------------------------------------------------------
-- Hôte :                        192.168.56.101
-- Version du serveur:           5.7.18-0ubuntu0.16.04.1 - (Ubuntu)
-- SE du serveur:                Linux
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la base pour loopback
CREATE DATABASE IF NOT EXISTS `loopback` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `loopback`;


-- Export de la structure de table loopback. mapping_codes
CREATE TABLE IF NOT EXISTS `mapping_codes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code_sz` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `code_type` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Export de données de la table loopback.mapping_codes : ~2 rows (environ)
/*!40000 ALTER TABLE `mapping_codes` DISABLE KEYS */;
INSERT INTO `mapping_codes` (`id`, `code_sz`, `code`, `code_type`, `created`) VALUES
	(1, 'sz_12121', 'bobo', 'ticker', '2017-03-28 13:19:47'),
	(2, 'sz_2222', 'chacha', 'ticker', '2017-03-28 13:19:47');
/*!40000 ALTER TABLE `mapping_codes` ENABLE KEYS */;


-- Export de la structure de table loopback. prices
CREATE TABLE IF NOT EXISTS `prices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `security` varchar(50) DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Export de données de la table loopback.prices : ~2 rows (environ)
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;
INSERT INTO `prices` (`id`, `security`, `price`) VALUES
	(2, 'vnm', 36.3),
	(3, 'nut', 30.3);
/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
